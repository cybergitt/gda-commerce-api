package com.digifreneur.gda.utils;

import java.util.UUID;

public class AppUtils {

    private AppUtils() {
        throw new UnsupportedOperationException("Cannot instantiate a AppUtils class");
    }

    public static String generateRandomUuid() {
        return UUID.randomUUID().toString();
    }
    
}
