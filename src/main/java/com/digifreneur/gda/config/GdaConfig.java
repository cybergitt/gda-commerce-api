package com.digifreneur.gda.config;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.NamingConventions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class GdaConfig {

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
    }

    // @Bean
    // public ModelMapper modelMapper() {
    //     ModelMapper modelMapper = new ModelMapper();
    //     modelMapper.getConfiguration().setFieldMatchingEnabled(true)
    //             .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
    //             .setSourceNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
    //     return modelMapper;
    //     // https://github.com/modelmapper/modelmapper/issues/212
    // }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Value("cors.allowedOrigins")
    private String allowedOrigins;

    public void addCorsMappings(CorsRegistry registry) {
        final long MAX_AGE_SECS = 3600000;

        registry.addMapping("/**").allowedOrigins(allowedOrigins).allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*").maxAge(MAX_AGE_SECS);
    }
}
