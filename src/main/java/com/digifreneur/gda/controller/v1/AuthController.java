package com.digifreneur.gda.controller.v1;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.digifreneur.gda.exception.AppException;
import com.digifreneur.gda.exception.GdaApiException;
import com.digifreneur.gda.model.Role;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.User;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.model.payload.JwtAuthenticationResponse;
import com.digifreneur.gda.model.payload.request.LoginRequest;
import com.digifreneur.gda.model.payload.request.SignUpRequest;
import com.digifreneur.gda.repository.RoleRepository;
import com.digifreneur.gda.repository.UserRepository;
import com.digifreneur.gda.security.JwtTokenProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
    private static final String USER_ROLE_NOT_SET = "User role not set";

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if ( Boolean.TRUE.equals(
            userRepository.existsByEmail(signUpRequest.getEmail())
            )
        ) {
            throw new GdaApiException(HttpStatus.BAD_REQUEST, "Email is already exist");
        }

        if (Boolean.TRUE.equals(userRepository.existsByPhone(signUpRequest.getPhone()))) {
            throw new GdaApiException(HttpStatus.BAD_REQUEST, "Phone Number is already taken");
        }

        String firstName = signUpRequest.getFirstName().toLowerCase();

        String lastName = signUpRequest.getLastName().toLowerCase();

        String email = signUpRequest.getEmail().toLowerCase();

        String password = passwordEncoder.encode(signUpRequest.getPassword());

        String phone = signUpRequest.getPhone().toLowerCase();

        String gender = signUpRequest.getGender().toUpperCase();

        String dob = signUpRequest.getDob().toLowerCase();

        //convert String to LocalDate yyyy-MM-dd
        LocalDate dateOBirth = LocalDate.parse(dob);

        User user = new User(email, password, firstName, lastName, phone, gender, dateOBirth);

        List<Role> roles = new ArrayList<>();

        if (userRepository.count() == 0) {
            roles.add(roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new AppException(USER_ROLE_NOT_SET)));
            roles.add(roleRepository.findByName(RoleName.ROLE_ADMIN)
                    .orElseThrow(() -> new AppException(USER_ROLE_NOT_SET)));
        } else {
            roles.add(roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new AppException(USER_ROLE_NOT_SET)));
        }

        user.setRoles(roles);

        User result = userRepository.save(user);

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/v1/user/{userId}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(Boolean.TRUE, "User registered successfully"));
    }

    @PostMapping("/generatepassword")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> encodePassword(String plainTextPassword) {
        String encoded = passwordEncoder.encode(plainTextPassword);
        return ResponseEntity.ok(encoded);
    }
}
