package com.digifreneur.gda.controller.v1;

import java.util.List;

import com.digifreneur.gda.model.Brand;
import com.digifreneur.gda.service.BrandService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/brand")
public class BrandController {
    private final Logger log = LoggerFactory.getLogger(BrandController.class);

    @Autowired
    private BrandService brandService;

    @GetMapping
    public ResponseEntity<List<Brand>> getAllBrand() {
        log.debug("REST request to get Brands");
        List<Brand> brands = brandService.getAllBrands();
        return new ResponseEntity<>(brands, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Brand> getBrand(@PathVariable(name = "id") Long id) {
        log.debug("REST Request to get Brand : {}", id);
        Brand brand = brandService.getBrand(id);
        return new ResponseEntity<>(brand, HttpStatus.OK);
    }
}
