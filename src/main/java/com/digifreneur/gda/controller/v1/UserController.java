package com.digifreneur.gda.controller.v1;

import java.util.stream.Collectors;

import com.digifreneur.gda.exception.GdaApiException;
import com.digifreneur.gda.model.payload.UserIdentityAvailability;
import com.digifreneur.gda.model.payload.UserProfile;
import com.digifreneur.gda.model.payload.UserSummary;
import com.digifreneur.gda.security.CurrentUser;
import com.digifreneur.gda.security.UserPrincipal;
import com.digifreneur.gda.service.UserService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final Logger log = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value(value = "${app.key}")
    private String appKey;

    @GetMapping("/me")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<UserSummary> getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        // log.info("User Role: " + currentUser.getAuthorities().stream().map(Object::toString).collect(Collectors.joining(",")));
        UserSummary userSummary = userService.getCurrentUser(currentUser);
        return new ResponseEntity<>(userSummary, HttpStatus.OK);
    }

    @GetMapping("/checkPhoneAvailability")
    public ResponseEntity<UserIdentityAvailability> checkPhoneAvailability(
            @RequestParam(value = "phone") String phone) {
        UserIdentityAvailability userIdentityAvailability = userService.checkPhoneAvailability(phone);

        return new ResponseEntity<>(userIdentityAvailability, HttpStatus.OK);
    }

    @GetMapping("/checkEmailAvailability")
    public ResponseEntity<UserIdentityAvailability> checkEmailAvailability(
            @RequestParam(value = "email") String email) {
        UserIdentityAvailability userIdentityAvailability = userService.checkEmailAvailability(email);
        return new ResponseEntity<>(userIdentityAvailability, HttpStatus.OK);
    }

    @GetMapping("/{username}/profile")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<UserProfile> getUserProfile(@PathVariable(value = "username") String usernameEnc, UserPrincipal currentUser ) { 
        
        try {
            // format usernameEnc = "${appKey}${ddmmyyyy}${phoneNum}"
            String username = usernameEnc.length() < 13 ? "" : usernameEnc.replace(appKey, "").substring(8).trim();

            log.info("Username: " + username);

            if (StringUtils.isNotBlank(username)) {
                UserProfile userProfile = userService.getUserProfile(username, currentUser);
                return new ResponseEntity<>(userProfile, HttpStatus.OK);
            }
        } catch (Exception e) {
            throw new GdaApiException(HttpStatus.BAD_REQUEST, "Error:" + e.getMessage());
        }
        return null;
    }
}
