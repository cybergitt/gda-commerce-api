package com.digifreneur.gda.controller.v1;

import java.util.List;

import javax.validation.Valid;

import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.Category;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.security.CurrentUser;
import com.digifreneur.gda.security.UserPrincipal;
import com.digifreneur.gda.service.CategoryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    private final Logger log = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    CategoryService categoryService;

    @GetMapping
    public ResponseEntity<List<Category>> getAllCategory() {
        log.debug("REST request to get Categories");
        List<Category> categories = categoryService.getAllCategories();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }    

    @GetMapping("/{id}")
    public ResponseEntity<Category> getCategory(@PathVariable(name = "id") Long id) {
        log.debug("REST Request to get Category : {}", id);
        Category category = categoryService.getCategory(id);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Category> addCategory(@Valid @RequestBody Category category, @CurrentUser UserPrincipal currentUser) {
        Category newCategory = categoryService.addCategory(category, currentUser);
        return new ResponseEntity<>(newCategory, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Category> updateCategory(@PathVariable(name = "id") Long id,
            @Valid @RequestBody Category category, @CurrentUser UserPrincipal currentUser)
            throws UnauthorizedException {
        Category updatedCategory = categoryService.updateCategory(id, category, currentUser);
        return new ResponseEntity<>(updatedCategory, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ApiResponse> deleteCategory(@PathVariable(name = "id") Long id,
            @CurrentUser UserPrincipal currentUser) throws UnauthorizedException {
        ApiResponse response = categoryService.deleteCategory(id, currentUser);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
