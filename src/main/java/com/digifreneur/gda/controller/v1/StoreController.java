package com.digifreneur.gda.controller.v1;

import java.util.List;

import com.digifreneur.gda.model.Store;
import com.digifreneur.gda.service.StoreService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/store")
public class StoreController {

    private final Logger log = LoggerFactory.getLogger(StoreController.class);

    @Autowired
    private StoreService storeService;

    @GetMapping
    public ResponseEntity<List<Store>> getAllStore() {
        log.debug("REST request to get Stores");
        List<Store> stores = storeService.getAllStores();
        return new ResponseEntity<>(stores, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Store> getStore(@PathVariable(name = "id") Long id) {
        log.debug("REST Request to get Store : {}", id);
        Store store = storeService.getStore(id);
        return new ResponseEntity<>(store, HttpStatus.OK);
    }
}
