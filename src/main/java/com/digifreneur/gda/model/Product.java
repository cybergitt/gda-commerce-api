package com.digifreneur.gda.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.digifreneur.gda.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.hibernate.annotations.NaturalId;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Table(
    name = "products", 
    uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }) }
)
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class, property = "id"
)
@JsonPropertyOrder({ "id", "code", "name", "price", "description", "store", "brand", "categories", "createdAt", "updatedAt" })
public class Product extends DateAudit{

    private static final long serialVersionUID = 7658664144399458744L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @NaturalId
    @Size(max = 12)
    @Column(name = "code", nullable = false, length = 25)
    private String code;

    @NotBlank
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @NotBlank
    @Column(name = "price", precision = 10, scale = 2, nullable = false)
    private BigDecimal price;

    @Lob
    @Column(name = "description", nullable = true)
    private String description;

    // @JsonIgnore
    // @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "store_id", nullable = false) // Optional
    // @JsonBackReference
    private Store store;

    // @JsonIgnore
    // @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brand_id", nullable = false) // Optional
    private Brand brand;

    // @JsonIgnore
    // @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
        name = "product_categories", 
        joinColumns = @JoinColumn(
            name = "product_id", referencedColumnName = "id", nullable = false, updatable = false), 
        inverseJoinColumns = @JoinColumn(
            name = "category_id", referencedColumnName = "id", nullable = false, updatable = false)
    )
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories == null ? null : new ArrayList<>(categories);
    }

    public void setCategories(List<Category> categories) {
        if (categories == null) {
            this.categories = null;
        } else {
            this.categories = Collections.unmodifiableList(categories);
        }
    }

}
