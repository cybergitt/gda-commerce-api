package com.digifreneur.gda.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.digifreneur.gda.model.audit.DateAudit;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Table(name = "stores")
@JsonPropertyOrder({ "id", "name", "description", "createdAt", "updatedAt" })
public class Store extends DateAudit {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotBlank
    @Column(name = "name", nullable = false, length = 100)
    @Size(max = 40)
    private String name;

    @Lob
    @Column(name = "description", nullable = true)
    private String description;

    @JsonIgnore
    // @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "store")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "store")
    // @JsonManagedReference
    private List<Product> products;

    public Store(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public List<Product> getProducts() {
        return this.products == null ? null : new ArrayList<>(this.products);
    }

    public void setProducts(List<Product> products) {
        if (products == null) {
            this.products = null;
        } else {
            this.products = Collections.unmodifiableList(products);
        }
    }
}
