package com.digifreneur.gda.model.payload;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.Data;

@Data
public class ProductDetailResponse {
    private Long id;
    private String code;
    private String name;
    private BigDecimal price;
    private String storeName;
    private String brandName;
    private List<String> categories;
    private String description;

    public List<String> getCategories() {
        return categories == null ? null : new ArrayList<>(categories);
    }

    public void setCategories(List<String> categories) {
        if (categories == null) {
            this.categories = null;
        } else {
            this.categories = Collections.unmodifiableList(categories);
        }
    }
}
