package com.digifreneur.gda.model.payload;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProfile {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String dob;
    private String gender;
    private String phone;
}
