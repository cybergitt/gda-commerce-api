package com.digifreneur.gda.model.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserSummary {
    private Long id;
    private String email;
    private String firstName;
    private String lastName;
    private String phone;
}
