package com.digifreneur.gda.model.payload.request;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ProductRequest {

    @NotBlank
    @Size(min = 4)
    private String code;

    @NotBlank
    private BigDecimal price;

    @NotNull
    private Long storeId;

    @NotNull
    private Long brandId;

    @Size(min = 5)
    private List<String> categories;

    @Size(min = 10)
    private String description;

    public List<String> getCategories() {

        return categories == null ? Collections.emptyList() : new ArrayList<>(categories);
    }

    public void setCategories(List<String> categories) {

        if (categories == null) {
            this.categories = null;
        } else {
            this.categories = Collections.unmodifiableList(categories);
        }
    }
}
