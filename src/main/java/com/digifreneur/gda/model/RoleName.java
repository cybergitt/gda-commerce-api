package com.digifreneur.gda.model;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER
}
