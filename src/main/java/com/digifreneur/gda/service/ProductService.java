package com.digifreneur.gda.service;

import java.util.ArrayList;
import java.util.List;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.Category;
import com.digifreneur.gda.model.Product;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.model.payload.ProductDetailResponse;
import com.digifreneur.gda.repository.CategoryRepository;
import com.digifreneur.gda.repository.ProductRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    
    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<Product> getAllProducts() {
        log.debug("Request to get all Products");
        return productRepository.findAll();
    }

    public Product getProduct(Long id) {
        log.debug("Request to get Product : {}", id);
        Product product = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        return product;
    }

    public ProductDetailResponse getProductDetailInfo(Long id) {
        log.debug("Request to get Product Detail Information : {}", id);
            Product product = productRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
            return convertToDto(product);
    }

    public Product addProduct(Product product, UserPrincipal currentUser) {
        log.debug("Request to add Product");

        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            Product newProduct = productRepository.save(product);
            return newProduct;
        }
        throw new UnauthorizedException("You don't have permission to add User");
    }

    public Product updateProduct(Long id, Product newProduct, UserPrincipal currentUser) {
        log.debug("Request to update Product : {}", id);
        Product product = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))
                || currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_USER.toString()))) {
            product.setCode(newProduct.getCode());
            product.setPrice(newProduct.getPrice());
            product.setDescription(newProduct.getDescription());
            Product updatedUser = productRepository.save(product);
            return updatedUser;
        }

        throw new UnauthorizedException("You don't have permission to edit this Product");
    }

    public ApiResponse deleteProduct(Long id, UserPrincipal currentUser) {
        log.debug("Request to delete User : {}", id);
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            productRepository.deleteById(id);
            return new ApiResponse(Boolean.TRUE, "You successfully deleted Product");
        }
        throw new UnauthorizedException("You don't have permission to delete this Product");
    }

    private ProductDetailResponse convertToDto(Product product) {
        // ProductDetailResponse productDetailResponse = modelMapper.map(product, ProductDetailResponse.class);
        ProductDetailResponse productDetailResponse = new ProductDetailResponse();
            // productDetailResponse.setStoreName( storeRepository.findById( product.getId() ).get().getName() );
            productDetailResponse.setId(product.getId());
            productDetailResponse.setName(product.getName());
            productDetailResponse.setCode(product.getCode());
            productDetailResponse.setPrice(product.getPrice());
            productDetailResponse.setStoreName(product.getStore().getName());
            productDetailResponse.setBrandName(product.getBrand().getName());
            List<String> categoryNames = new ArrayList<>(product.getCategories().size());
            for (Category category : product.getCategories()) {
                categoryNames.add(category.getName());
            }
            productDetailResponse.setCategories(categoryNames);
            productDetailResponse.setDescription(product.getDescription());
        return productDetailResponse;
    }

    // private Product convertToEntity(ProductDto productDto) throws ParseException {
    //     Product product = modelMapper.map(productDto, Product.class);
    //     product.setSubmissionDate(
    //             productDto.getSubmissionDateConverted(userService.getCurrentUser().getPreference().getTimezone()));

    //     if (productDto.getId() != null) {
    //         Product oldProduct = productService.getPostById(productDto.getId());
    //         product.setRedditID(oldProduct.getRedditID());
    //         product.setSent(oldProduct.isSent());
    //     }
    //     return product;
    // }
}
