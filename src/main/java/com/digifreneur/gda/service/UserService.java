package com.digifreneur.gda.service;

import java.time.format.DateTimeFormatter;
import java.util.List;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.User;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.model.payload.UserIdentityAvailability;
import com.digifreneur.gda.model.payload.UserProfile;
import com.digifreneur.gda.model.payload.UserSummary;
import com.digifreneur.gda.repository.UserRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<User> getAllUsers(UserPrincipal currentUser) {
        log.debug("Request to get all Users");
        if ( currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString())) ) {
            return userRepository.findAll();
        }
        throw new UnauthorizedException("You don't have permission to view Users");
    }

    public UserSummary getCurrentUser(UserPrincipal currentUser) {
        log.info("Request to get User Profile : {}", currentUser.getEmail());
        return new UserSummary(
            currentUser.getId(),
            currentUser.getEmail(),
            currentUser.getFirstName(),
            currentUser.getLastName(),
            currentUser.getPhone()
        );
    }

    public UserIdentityAvailability checkEmailAvailability(String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

    public UserIdentityAvailability checkPhoneAvailability(String phone) {
        Boolean isAvailable = !userRepository.existsByPhone(phone);
        return new UserIdentityAvailability(isAvailable);
    }

    public User getUser(Long id, UserPrincipal currentUser) {
        log.debug("Request to get User : {}", id);
        if (
            currentUser.getAuthorities().contains(
                new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()) ) || 
            currentUser.getAuthorities().contains(
                new SimpleGrantedAuthority(RoleName.ROLE_USER.toString()) )
            ) {
            User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
            return user;
        }

        throw new UnauthorizedException("You don't have permission to view User");
    }

    public UserProfile getUserProfile(String username, UserPrincipal currentUser) {
        log.info("Request to get User Profile : {}", currentUser.getFirstName());
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))
                || currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_USER.toString()))) {
            User user = userRepository.findByPhone(
                    username).orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
            return convertToDto(user);
        }

        throw new UnauthorizedException("You don't have permission to view User");
    }

    public User addUser(User user, UserPrincipal currentUser) {
        log.debug("Request to add User");

        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            User newUser = userRepository.save(user);
            return newUser;
        }
        throw new UnauthorizedException("You don't have permission to add User");
    }

    public User updateUser(Long id, User newUser, UserPrincipal currentUser) {
        log.debug("Request to update User : {}", id);
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))
                || currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_USER.toString()))) {
            user.setEmail(newUser.getEmail());
            if ( newUser.getPassword() != null ) {
                user.setPassword(newUser.getPassword());
            }
            user.setFirstName(newUser.getFirstName());
            user.setLastName(newUser.getLastName());
            user.setGender(newUser.getGender());
            user.setDob(newUser.getDob());
            user.setPhone(newUser.getPhone());
            User updatedUser = userRepository.save(user);
            return updatedUser;
        }

        throw new UnauthorizedException("You don't have permission to edit this User");
    }

    public ApiResponse deleteUser(Long id, UserPrincipal currentUser) {
        log.debug("Request to delete User : {}", id);
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            userRepository.deleteById(id);
            return new ApiResponse(Boolean.TRUE, "You successfully deleted User");
        }
        throw new UnauthorizedException("You don't have permission to delete this User");
    }

    private UserProfile convertToDto(User user) {
        // UserProfile userProfile = modelMapper.map(user, UserProfile.class);
        UserProfile userProfile = new UserProfile();
        userProfile.setFirstName(user.getFirstName());
        userProfile.setLastName(user.getLastName());
        userProfile.setGender(user.getGender());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedDate= user.getDob().format(formatter);
        userProfile.setDob(formattedDate);
        userProfile.setPhone(user.getPhone());
        return userProfile;
    }
}
