package com.digifreneur.gda.service;

import java.util.List;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.Store;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.repository.StoreRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class StoreService {

    private final Logger log = LoggerFactory.getLogger(StoreService.class);

    @Autowired
    public StoreRepository storeRepository;

    public List<Store> getAllStores() {
        log.debug("Request to get all Stores");
        return storeRepository.findAll();
    }

    public Store getStore(Long id) {
        log.debug("Request to get Store : {}", id);
        Store store = storeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Store", "id", id));
        return store;
    }

    public Store addStore(Store store, UserPrincipal currentUser) {
        log.debug("Request to add Store");
        Store newStore = storeRepository.save(store);
        return newStore;
    }

    public Store updateStore(Long id, Store newStore, UserPrincipal currentUser) {
        log.debug("Request to update Store : {}", id);
        Store store = storeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Store", "id", id));
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            store.setName(newStore.getName());
            Store updatedStore = storeRepository.save(store);
            return updatedStore;
        }

        throw new UnauthorizedException("You don't have permission to edit this Store");
    }

    public ApiResponse deleteStore(Long id, UserPrincipal currentUser) {
        log.debug("Request to delete Store : {}", id);
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            storeRepository.deleteById(id);
            return new ApiResponse(Boolean.TRUE, "You successfully deleted Store");
        }
        throw new UnauthorizedException("You don't have permission to delete this Store");
    }
}
