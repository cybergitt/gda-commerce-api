package com.digifreneur.gda.service;

import java.util.List;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.Brand;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.repository.BrandRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class BrandService {
    
    private final Logger log = LoggerFactory.getLogger(BrandService.class);

    @Autowired
    public BrandRepository brandRepository;

    public List<Brand> getAllBrands() {
        log.debug("Request to get all Brands");
        return brandRepository.findAll();
    }

    public Brand getBrand(Long id) {
        log.debug("Request to get Brand : {}", id);
        Brand brand = brandRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Brand", "id", id));
        return brand;
    }

    public Brand addBrand(Brand Brand, UserPrincipal currentUser) {
        log.debug("Request to add Brand");
        Brand newBrand = brandRepository.save(Brand);
        return newBrand;
    }

    public Brand updateBrand(Long id, Brand newBrand, UserPrincipal currentUser) {
        log.debug("Request to update Brand : {}", id);
        Brand brand = brandRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Brand", "id", id));
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            brand.setName(newBrand.getName());
            Brand updatedBrand = brandRepository.save(brand);
            return updatedBrand;
        }

        throw new UnauthorizedException("You don't have permission to edit this Brand");
    }

    public ApiResponse deleteBrand(Long id, UserPrincipal currentUser) {
        log.debug("Request to delete Brand : {}", id);
        if (currentUser.getAuthorities().contains(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))) {
            brandRepository.deleteById(id);
            return new ApiResponse(Boolean.TRUE, "You successfully deleted Brand");
        }
        throw new UnauthorizedException("You don't have permission to delete this Brand");
    }
}
