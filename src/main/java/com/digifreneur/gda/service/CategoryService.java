package com.digifreneur.gda.service;

import java.util.List;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.exception.UnauthorizedException;
import com.digifreneur.gda.model.Category;
import com.digifreneur.gda.model.RoleName;
import com.digifreneur.gda.model.payload.ApiResponse;
import com.digifreneur.gda.repository.CategoryRepository;
import com.digifreneur.gda.security.UserPrincipal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);
    
    @Autowired
    public CategoryRepository categoryRepository;

    public List<Category> getAllCategories() {
        log.debug("Request to get all Categories");
        return categoryRepository.findAll();
    }

    public Category getCategory(Long id) {
        log.debug("Request to get Category : {}", id);
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Category", "id", id));
        return category;
    }

    public Category addCategory(Category category, UserPrincipal currentUser) {
        log.debug("Request to add Category");
        Category newCategory = categoryRepository.save(category);
        return newCategory;
    }

    public Category updateCategory(Long id, Category newCategory, UserPrincipal currentUser) {
        log.debug("Request to update Category : {}", id);
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", id));
        if ( currentUser.getAuthorities().contains(
            new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))
            ) {
            category.setName(newCategory.getName());
            Category updatedCategory = categoryRepository.save(category);
            return updatedCategory;
        }

        throw new UnauthorizedException("You don't have permission to edit this category");
    }

    public ApiResponse deleteCategory(Long id, UserPrincipal currentUser) {
        log.debug("Request to delete Category : {}", id);
        if ( 
            currentUser.getAuthorities().contains(
                new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.toString()))
            ) {
            categoryRepository.deleteById(id);
            return new ApiResponse(Boolean.TRUE, "You successfully deleted category");
        }
        throw new UnauthorizedException("You don't have permission to delete this category");
    }
}
