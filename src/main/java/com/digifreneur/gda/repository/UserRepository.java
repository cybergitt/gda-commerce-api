package com.digifreneur.gda.repository;

import java.util.Optional;

import javax.validation.constraints.NotBlank;

import com.digifreneur.gda.exception.ResourceNotFoundException;
import com.digifreneur.gda.model.User;
import com.digifreneur.gda.model.payload.UserProfile;
import com.digifreneur.gda.security.UserPrincipal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    Optional<User> findByEmail(String email);

    Optional<User> findByPhone(String phone);

    Optional<User> findByEmailOrPhone(String email, String phone);

    Boolean existsByEmail(@NotBlank String email);

    Boolean existsByPhone(@NotBlank String phone);

    default User getUser(UserPrincipal currentUser) {
        return getUserByEmail(currentUser.getUsername());
    }

    default User getUserByEmail(String email) {
        return findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User", "email", email));
    }
}
