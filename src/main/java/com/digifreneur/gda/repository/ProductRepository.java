package com.digifreneur.gda.repository;

import java.util.List;
import java.util.Optional;

import com.digifreneur.gda.model.Category;
import com.digifreneur.gda.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    
    Optional<Product> findByCode(String code);

    Optional<Product> findByBrand(Long brandId);

    Optional<Product> findByStore(Long storeId);

    Long countByStoreId(Long storeId);
}
